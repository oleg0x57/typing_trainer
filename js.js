var Lesson = function () {
    this.lesson = [];
    this.isFinished = false;
    this.isStarted = false;
    this.pointer = 0;
    this.errors = 0;
};
Lesson.prototype.getChar = function (charCode) {
    return String.fromCharCode(charCode);
};
Lesson.prototype.init = function (startButton, stopButton, outputTo) {
    this.stopButton = document.getElementById(stopButton);
    this.startButton = document.getElementById(startButton);
    this.outputTo = document.getElementById(outputTo);
    document.addEventListener('keypress', this.checkInput.bind(this), false);
    this.startButton.addEventListener('click', this.startLesson.bind(this), false);
    this.stopButton.addEventListener('click', this.stopLesson.bind(this), false);
};
Lesson.prototype.loadText = function (text) {
    var splitText = text.split('');
    for (var i in splitText) {
        this.lesson.push({'char': splitText[i], 'state': 0});
    }
};
Lesson.prototype.currentChar = function () {
    return this.lesson[this.pointer];
};
Lesson.prototype.nextChar = function () {
    if (this.lastChar() === this.currentChar()) {
        this.isFinished = true;
    } else {
        this.pointer++;
    }
};
Lesson.prototype.lastChar = function () {
    return this.lesson[this.lesson.length - 1];
};
Lesson.prototype.isValidChar = function (input) {
    return input === this.currentChar().char;
};
Lesson.prototype.isLessonFinished = function () {
    return this.isFinished;
};
Lesson.prototype.renderChar = function (char) {
    var charElem = document.createElement('span');
    if (char['state'] === 1) {
        charElem.className = "char active";
    } else if (char['state'] === 2) {
        charElem.className = "char error";
    } else {
        charElem.className = "char";
    }
    charElem.innerHTML = char['char'];
    return charElem;
};
Lesson.prototype.renderLesson = function () {
    this.outputTo.innerHTML = '';
    for (var i in this.lesson) {
        this.outputTo.appendChild(this.renderChar(this.lesson[i]));
    }
};
Lesson.prototype.setActive = function (charId) {
    this.lesson[charId]['state'] = 1;
};
Lesson.prototype.setError = function (charId) {
    this.lesson[charId]['state'] = 2;
};
Lesson.prototype.checkInput = function () {
    if (!this.isStarted) {
        return;
    }
    var pressedKey = this.getChar(event.which);
    if (this.isValidChar(pressedKey)) {
        this.nextChar();
        this.setActive(this.pointer);
    } else {
        this.setError(this.pointer);
        this.errors++;
    }
    this.renderLesson();
    if (this.isLessonFinished()) {
        alert('Errors: ' + this.errors);
    }
};
Lesson.prototype.startLesson = function () {
    this.isStarted = true;
    this.isFinished = false;
    this.setActive(this.pointer);
    this.errors = 0;
    this.renderLesson();
};
Lesson.prototype.stopLesson = function () {
    this.isFinished = true;
    this.isStarted = false;
};

var lessonString = 'aa ss dd ff jj kk ll ;;';
var newLesson = new Lesson();
newLesson.loadText(lessonString);
newLesson.init('start', 'stop', 'lesson');
newLesson.renderLesson();